# Edits for main app

## Welcome Page

The welcome page contains grammatical errors: 

The first paragraph should have a period where the first comma is. 

In the second paragraph, ‘issue’ should be ‘issues’, and should end in a period where the first comma is.

In the final paragraph, ‘*an*’ should be changed to ‘*a*’

## Demo Mode

In the second paragraph: “…give recommendations” should be “…give you recommendations.”

In the third paragraph: “…or give recommendations for your parameters” could be reworded to “…or **to** give recommendations that reflect your parameters.” The sentence should also end in a period where the current first comma is.

## Terms and Privacy

The terms and privacy section incorrectly overflows its container on scroll.

Language selection allows multiple choice and does *not* deselect previously chosen language. (Perhaps this is a just a design limitation?)

Done button on 'language selection' page incorrectly scales… and stretches? This would look better as a subtle animation at most.

## Creating a password 

The first sentence under creating a password contains a grammatical error. “An” should be “a”.

##  Lets get started

The “Lets get started” page contains a grammatical error in the terms of service agreement: “…you agree with *out* terms of service” should be “…you agree with *our* terms of service”.

Under the “Earning $LONG” heading, the second paragraph contains a grammatical error: “You can use this token to get products and services in Longevity EcoSystem” should be “You can use this token to get products and services in **the** Longevity EcoSystem.” 

The next paragraph on the same page begins with: “Every time you see an {icon} icon…”. It should be: “Every time you see **the** {icon} icon…”

## We found these apps installed

The “We found these apps installed?” page should **not** have a question mark on the end of the title, *unless* it is intended to read as a question. 

The Garmin Connect app contains grammatical errors. “May take a few minute to retrieve all data, we’ll let you know when done” should be written:  “It may take a few minutes to retrieve all data. We’ll let you know when finished./!”

Rephrasing “your app is missing?” to “Don’t see your app?” would sound more natural.

## We found these devices nearby

On “We found these devices nearby” page, the Xiaomi Mi band page contains grammatical errors. “May take few minutes to retrieve all data, we’ll let you know when done” should be written:  “It may take a few minutes to retrieve all data. We’ll let you know when finished./!”

## We found these devices nearby

The skip button on the “We found these devices nearby” blends into the text, making the UI look broken. Perhaps a soft background color could fix this?

On the “Now we need to know more about you” page, there are grammatical errors. The sentence “Add basic profile information, you can choose to not inform some of the following by clicking on {icon} icon, but results may not be fully precise” should be written: “Add basic profile information. You can choose to not fill out some of the following fields by clicking on the {icon} icon, but results may not be fully precise”

(By this point, buttons seemed to have the same incorrect scaling behavior on hover **so frequently**, that it seemed improbable it was a mistake. I assumed it was a design limitation from here on out and skipped notating it.)

## More data, More precision

On the “More data, More precision” page, in the first paragraph “reward” should be changed to “rewarding”.

In the second paragraph the first sentence should have a period where the first comma is. ‘Include’ should be ‘include them’. Suggestion: rewording to ‘If you don’t have biomarkers or choose not to include them…” would be less vague.

On the ‘We are done’ page, the first paragraph should **not** be a singular long sentence with a comma. It should be two sentences, separated by a period where the comma currently is.

In the second paragraph ‘disease’ could be ‘diseases’.

## Dashboard

The “Home, Suggested, Risks” header in the dashboard section is fixed in the UI, but has no background color. Accordingly, when text is scrolled over it, the UI looks broken. This become very obvious when selecting the “All” link of suggestions on the dashboard home. Perhaps a continuous background color whether toggled or not could address this?

## Biomarker Analysis & Risks 

There is small typo where “Changes” is spelled “Chnges”. 

## Library 

In the header sentence “Browse trough” should be “Browse through”. “Evidences” should not be plural, and should be be “evidence”. Perhaps rewording to “Browse through scientific evidence that is the foundation of our products.” would be more concise and clear.
